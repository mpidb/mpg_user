-- run: mysql -u root -p mpidb_mpg_user < updateDB_0500.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

USE mpidb_mpg_user;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- initial value
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0500');
END IF;

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '0500' THEN
 LEAVE proc_label;
END IF;

-- CHANGES V0.5.01 :
-- *****************
-- fs::rsync      - initiales FS

IF ( SELECT MAX(version) FROM dataface__version ) < '0501' THEN

  -- initiale DB

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0501');
END IF;


-- CHANGES V0.5.02 :
-- *****************
-- fs::list_telecom    - select bei tcom raus
-- db::list_typ        - add table

IF ( SELECT MAX(version) FROM dataface__version ) < '0502' THEN

  -- alte views delete 
  DROP VIEW view_fax;
  DROP VIEW view_mobile;
  DROP VIEW view_phone;

  -- add table list_typ
  CREATE TABLE IF NOT EXISTS list_typ (
   typ varchar(15) COLLATE utf8_unicode_ci NOT NULL, PRIMARY KEY (typ)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  INSERT INTO `list_typ` (`typ`) VALUES ('fax'),('mobile'),('phone');


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0502');
END IF;


-- CHANGES V0.5.03 - 2016-04-27
-- ****************************
 -- fs::mpi_user    - select abteilung
 -- db::mpi_user    - add field abteilung

 IF ( SELECT MAX(version) FROM dataface__version ) < '0503' THEN

  ALTER TABLE mpi_user ADD abteilung VARCHAR(30) NULL AFTER kostenstelle ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0503');
 END IF;


-- CHANGES V0.5.04 - 2016-06-09
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver

 IF ( SELECT MAX(version) FROM dataface__version ) < '0504' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0504');
 END IF;


-- CHANGES V0.5.10 - 2018-08-14
-- ****************************
-- UPDATE: change Autorisierung von mpi_user nach sys_user mit Rollentabelle
-- db::list_reiter - Aenderung tabellen anpassen
-- fs::sys_user,list_rolle - Anpassung neue Benutzerverwaltung

IF ( SELECT MAX(version) FROM dataface__version ) < '0510' THEN

   -- create table list_role
    -- DROP TABLE IF EXISTS `list_role`;
    CREATE TABLE IF NOT EXISTS `list_role` (
      `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
      `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- insert default roles
    INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
    (000001, 'NO ACCESS', 'No_Access'),
    (000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
    (000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
    (000004, 'DELETE', 'EDIT and delete and delete found'),
    (000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
    (000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
    (000007, 'USER', 'READ_ONLY and add new related record'),
    (000008, 'ADMIN', 'DELETE and xml_view'),
    (000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install');
    -- unique, pri
    ALTER TABLE `list_role` ADD PRIMARY KEY IF NOT EXISTS (`rolID`), ADD UNIQUE KEY IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `list_role` MODIFY `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

    -- create table sys_user
    -- DROP TABLE IF EXISTS `sys_user`;
    CREATE TABLE IF NOT EXISTS `sys_user` (
     `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
     `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
     `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NULL,
     `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- unique, pri
    ALTER TABLE `sys_user`
     ADD PRIMARY KEY IF NOT EXISTS (`logID`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `login` (`login`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `email` (`email`),
     ADD KEY         IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `sys_user` MODIFY `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
    -- Constraint
    ALTER TABLE `sys_user` ADD CONSTRAINT `sysUser_listRole` FOREIGN KEY IF NOT EXISTS (`role`) REFERENCES `list_role` (`role`);

    -- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
    CREATE OR REPLACE VIEW view_user AS
     SELECT
      employee_id AS userID,
      localID AS login,
      CONCAT(last_name, ', ', first_name, ' (', IFNULL(localID,'--'), ')') AS sort
     FROM
      mpi_user
     WHERE
      active = 1
     ORDER BY
      login
     ;

    -- CREATE OR REPLACE VIEW view_user AS
    --  SELECT
    --   '000001' AS userID, 'mpg_local' AS login, 'MPG, version (mpg_local)' AS sort;

    -- create if not exist list_reiter
    CREATE TABLE IF NOT EXISTS `list_reiter` (
     `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
     `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `favorit` tinyint(1) NOT NULL DEFAULT '0',
     `history` tinyint(1) NOT NULL DEFAULT '0',
     `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `reiter` (`reiter`),
     KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

    -- create if not exist list_katReiter
    CREATE TABLE IF NOT EXISTS `list_katReiter` (
     `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;
    -- add entries in list_katReiter
    UPDATE list_katReiter SET kategorie = 'Autorisierung' WHERE kategorie = 'Authorisierung';
    INSERT IGNORE INTO `list_katReiter` (`kategorie`) VALUES ('Autorisierung');

    -- add entries in list_reiter
    INSERT IGNORE INTO `list_reiter` (`reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
     ('sys_user', 'Autorisierung', 1, 1, 'Autorisierung und Berechtigung Benutzer'),
     ('list_role', 'Autorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
    UPDATE `list_reiter` SET `bedeutung` = 'Auswahlliste fuer aktive und nicht abgelaufene Benutzer' WHERE `reiter` = 'view_user';

    -- copy inserts from old mpi_users
    INSERT IGNORE INTO sys_user (login, password, role, email, bearbeiter, zeitstempel) SELECT username, password, role, email, 'import', zeitstempel FROM mpi_users;

    -- del old table mpi_users (if all done and work)
    DROP TABLE IF EXISTS `mpi_users`;
    DROP TABLE IF EXISTS `mpi_users__history`;
    DELETE FROM `list_reiter` WHERE `reiter` = 'mpi_users';


 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('0510') ;

END IF;


END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();
