
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mpidb_mpg_user` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `mpidb_mpg_user`;
DROP TABLE IF EXISTS `dataface__failed_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__failed_logins` (
  `attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `time_of_attempt` int(11) NOT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__failed_logins` WRITE;
/*!40000 ALTER TABLE `dataface__failed_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__failed_logins` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__index` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(64) NOT NULL,
  `record_id` varchar(255) NOT NULL,
  `record_url` varchar(255) NOT NULL,
  `record_title` varchar(255) NOT NULL,
  `record_description` text,
  `lang` varchar(2) NOT NULL,
  `searchable_text` text,
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `record_key` (`record_id`,`lang`),
  FULLTEXT KEY `searchable_text_index` (`searchable_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__index` WRITE;
/*!40000 ALTER TABLE `dataface__index` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__index` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__modules` (
  `module_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__modules` WRITE;
/*!40000 ALTER TABLE `dataface__modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__modules` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__mtimes` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__preferences` (
  `pref_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pref_id`),
  KEY `username` (`username`),
  KEY `table` (`table`),
  KEY `record_id` (`record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__preferences` WRITE;
/*!40000 ALTER TABLE `dataface__preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__preferences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__record_mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__record_mtimes` (
  `recordhash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `recordid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`recordhash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__record_mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__record_mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__record_mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__version` (
  `version` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__version` WRITE;
/*!40000 ALTER TABLE `dataface__version` DISABLE KEYS */;
INSERT INTO `dataface__version` VALUES (510);
/*!40000 ALTER TABLE `dataface__version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_katReiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_katReiter` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_katReiter` WRITE;
/*!40000 ALTER TABLE `list_katReiter` DISABLE KEYS */;
INSERT INTO `list_katReiter` VALUES (5,'Ablage');
INSERT INTO `list_katReiter` VALUES (7,'Auswertung');
INSERT INTO `list_katReiter` VALUES (4,'Autorisierung');
INSERT INTO `list_katReiter` VALUES (1,'Haupttabelle');
INSERT INTO `list_katReiter` VALUES (8,'History');
INSERT INTO `list_katReiter` VALUES (2,'Liste');
INSERT INTO `list_katReiter` VALUES (9,'Programmierung');
INSERT INTO `list_katReiter` VALUES (3,'View');
INSERT INTO `list_katReiter` VALUES (6,'Zuordnung');
/*!40000 ALTER TABLE `list_katReiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_kostenstelle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_kostenstelle` (
  `kostenstelle` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kostenstelle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_kostenstelle` WRITE;
/*!40000 ALTER TABLE `list_kostenstelle` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_kostenstelle` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_reiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `history` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=435 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_reiter` WRITE;
/*!40000 ALTER TABLE `list_reiter` DISABLE KEYS */;
INSERT INTO `list_reiter` VALUES (000002,'mpi_user','Haupttabelle',1,1,'Tabelle Benutzer');
INSERT INTO `list_reiter` VALUES (000006,'list_katReiter','Liste',0,0,'Zugehoerigkeit DB-Tabellen');
INSERT INTO `list_reiter` VALUES (000016,'list_reiter','Liste',0,0,'Sammelcontainer fuer Tabbutton \"mehr ..\"');
INSERT INTO `list_reiter` VALUES (000416,'view_favorit','View',0,0,'Menueeintrag in Favorit fuer schnelleren Zugriff');
INSERT INTO `list_reiter` VALUES (000419,'view_reiter','Programmierung',0,0,'Hole alle Tabellen von Datenbank von mysql');
INSERT INTO `list_reiter` VALUES (000429,'list_telecom','Liste',0,0,'Telefon');
INSERT INTO `list_reiter` VALUES (000430,'list_kostenstelle','Liste',0,0,'Kostenstellen');
INSERT INTO `list_reiter` VALUES (000431,'mpi_group','Haupttabelle',0,0,'Gruppen');
INSERT INTO `list_reiter` VALUES (000432,'mpi_user_group','Liste',0,0,'Multigruppen');
INSERT INTO `list_reiter` VALUES (000433,'sys_user','Autorisierung',1,1,'Autorisierung und Berechtigung Benutzer');
INSERT INTO `list_reiter` VALUES (000434,'list_role','Autorisierung',1,0,'Liste aller Berechtigungen (Rollen)');
/*!40000 ALTER TABLE `list_reiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_role` (
  `rolID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rolID`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_role` WRITE;
/*!40000 ALTER TABLE `list_role` DISABLE KEYS */;
INSERT INTO `list_role` VALUES (000001,'NO ACCESS','No_Access');
INSERT INTO `list_role` VALUES (000002,'READ ONLY','view, list, calendar, view xml, show all, find, navigate');
INSERT INTO `list_role` VALUES (000003,'EDIT','READ_ONLY and edit, new record, remove, import, translate, copy');
INSERT INTO `list_role` VALUES (000004,'DELETE','EDIT and delete and delete found');
INSERT INTO `list_role` VALUES (000005,'OWNER','DELETE except navigate, new, and delete found');
INSERT INTO `list_role` VALUES (000006,'REVIEWER','READ_ONLY and edit and translate');
INSERT INTO `list_role` VALUES (000007,'USER','READ_ONLY and add new related record');
INSERT INTO `list_role` VALUES (000008,'ADMIN','DELETE and xml_view');
INSERT INTO `list_role` VALUES (000009,'MANAGER','ADMIN and manage, manage_migrate, manage_build_index, and install');
/*!40000 ALTER TABLE `list_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_telecom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_telecom` (
  `autoID` int(11) NOT NULL AUTO_INCREMENT,
  `localID` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tcom` enum('phone','mobile','fax') COLLATE utf8_unicode_ci NOT NULL,
  `tcom_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `localID` (`localID`,`tcom`,`tcom_number`),
  CONSTRAINT `list_telecom_ibfk_3` FOREIGN KEY (`localID`) REFERENCES `mpi_user` (`localID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_telecom` WRITE;
/*!40000 ALTER TABLE `list_telecom` DISABLE KEYS */;
INSERT INTO `list_telecom` VALUES (1,'muster','phone','12345');
INSERT INTO `list_telecom` VALUES (2,'muster','mobile','67890');
INSERT INTO `list_telecom` VALUES (3,'muster','fax','455465673');
/*!40000 ALTER TABLE `list_telecom` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_typ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_typ` (
  `typ` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`typ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_typ` WRITE;
/*!40000 ALTER TABLE `list_typ` DISABLE KEYS */;
INSERT INTO `list_typ` VALUES ('fax');
INSERT INTO `list_typ` VALUES ('mobile');
INSERT INTO `list_typ` VALUES ('phone');
/*!40000 ALTER TABLE `list_typ` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_group` (
  `gruppe` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`gruppe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_group` WRITE;
/*!40000 ALTER TABLE `mpi_group` DISABLE KEYS */;
INSERT INTO `mpi_group` VALUES ('Mustergruppe');
/*!40000 ALTER TABLE `mpi_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_user` (
  `localID` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `street_address` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` enum('en','de') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `employeed_since` date DEFAULT NULL,
  `employeed_until` date DEFAULT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `room` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kostenstelle` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abteilung` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`localID`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_user` WRITE;
/*!40000 ALTER TABLE `mpi_user` DISABLE KEYS */;
INSERT INTO `mpi_user` VALUES ('muster','123456','Muster','Max',NULL,NULL,NULL,NULL,NULL,NULL,'en',1,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2016-02-07 17:28:53');
/*!40000 ALTER TABLE `mpi_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_user_group` (
  `groupname` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `localID` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`groupname`),
  UNIQUE KEY `groupname` (`groupname`,`localID`),
  KEY `localID` (`localID`),
  CONSTRAINT `mpi_user_group_ibfk_4` FOREIGN KEY (`localID`) REFERENCES `mpi_user` (`localID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mpi_user_group_ibfk_5` FOREIGN KEY (`groupname`) REFERENCES `mpi_group` (`gruppe`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_user_group` WRITE;
/*!40000 ALTER TABLE `mpi_user_group` DISABLE KEYS */;
INSERT INTO `mpi_user_group` VALUES ('Mustergruppe','muster');
/*!40000 ALTER TABLE `mpi_user_group` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `logID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`logID`) USING BTREE,
  UNIQUE KEY `login` (`login`) USING BTREE,
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`),
  CONSTRAINT `sysUser_listRole` FOREIGN KEY (`role`) REFERENCES `list_role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (000001,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','MANAGER','','import','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `view_favorit`;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_favorit` (
  `autoID` tinyint NOT NULL,
  `reiter` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `favorit` tinyint NOT NULL,
  `history` tinyint NOT NULL,
  `bedeutung` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_reiter`;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_reiter` (
  `reiter` tinyint NOT NULL,
  `table_type` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_user`;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_user` (
  `userID` tinyint NOT NULL,
  `login` tinyint NOT NULL,
  `sort` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

USE `mpidb_mpg_user`;
/*!50001 DROP TABLE IF EXISTS `view_favorit`*/;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_favorit` AS select `list_reiter`.`autoID` AS `autoID`,`list_reiter`.`reiter` AS `reiter`,`list_reiter`.`kategorie` AS `kategorie`,`list_reiter`.`favorit` AS `favorit`,`list_reiter`.`history` AS `history`,`list_reiter`.`bedeutung` AS `bedeutung` from `list_reiter` where (`list_reiter`.`favorit` = '1') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_reiter`*/;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reiter` AS select (convert(`information_schema`.`tables`.`TABLE_NAME` using utf8) collate utf8_unicode_ci) AS `reiter`,`information_schema`.`tables`.`TABLE_TYPE` AS `table_type` from `information_schema`.`tables` where ((`information_schema`.`tables`.`TABLE_SCHEMA` = (select database())) and ((`information_schema`.`tables`.`TABLE_TYPE` = 'base table') or (`information_schema`.`tables`.`TABLE_TYPE` = 'view'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_user`*/;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_user` AS select `mpi_user`.`employee_id` AS `userID`,`mpi_user`.`localID` AS `login`,concat(`mpi_user`.`last_name`,', ',`mpi_user`.`first_name`,' (',ifnull(`mpi_user`.`localID`,'--'),')') AS `sort` from `mpi_user` where (`mpi_user`.`active` = 1) order by `mpi_user`.`localID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

