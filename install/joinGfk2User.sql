-- Aenderungen fuer Weitergabe einer global gueltigen DB mit Kopplung mpg_user -> mpg_gfk
-- wird benoetigt, wenn man eine Userauswahlliste fuer alle DB's haben moechte
-- seperat auszufuehrende sql-datei
-- view zwischen den DB's sollte vorher als fake existieren, damit das Filesytem nicht mehr geaendert werden muss
-- view_user (mpg_gfk)


-- DB Gefahrenmerkmale
CREATE OR REPLACE VIEW mpidb_mpg_gfk.view_user AS
SELECT
 localID, last_name, first_name FROM mpidb_mpg_user.mpi_user
WHERE
 active = 1
ORDER BY
 localID
;

