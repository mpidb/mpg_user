<?php

class tables_mpi_user { 

  function block__before_fineprint() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $mailto = $app->_conf['_own']['mailto'];
    $mname  = $app->_conf['_own']['mailname'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep == $db_ver )
      echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> = <span style="color: green">DB: '.$db_ver.'</span> &copy;<a href="mailto:'.$mailto.'">'.$mname.'</a>';
  }

  function block__before_body() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep != $db_ver ) {
      if ( $fs_rep > $db_ver ) {
        echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> &ne; <span style="color: red">DB: '.$db_ver.'</span>';
      } else {
        echo 'Version <span style="color: red">FS: '.$fs_ver.'</span> &ne; <span style="color: green">DB: '.$db_ver.'</span>';
      }
    }
  }

  function block__after_result_list_content() {
    echo 'Benutzer &nbsp;';
    echo '<span style="background-color:#cfc;"> ist aktiv. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffdc99;"> und hat Telefon. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#fcc;"> ist inaktiv. </span>&nbsp;&nbsp;&nbsp;&nbsp;';
  }

  // setze Farbe in listenansicht entsprechend Status
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    if ($record->val('active') == '0') return $table.'rot';
    $tel   = $record->getRelatedRecordObjects('telecom');
    if ($tel) return $table.'gelb';
    return $table;
  }

  function getTitle(&$record)  {
    $fname = $record->display('first_name');
    $sname = $record->display('last_name');
    return $sname.', '.$fname;
  }

  function active__display(&$record) { 
    if ($record->val('active') == '1') return 'ja';
    return 'nein';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record)  {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
