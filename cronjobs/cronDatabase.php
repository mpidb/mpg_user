<?php

// Cronjob Skript fuer Aktionen ausserhalb der DB
// z.B. Mailbenachrichtigung, Loeschen verwaister Ablagen etc.
// Beispiele folgend
// schachi 2016-06-28

  // stelle sicher das dieses Skript in einem Subdir liegt, normalerweise im Ordner cronjobs, sonst gibt es kausale Problem :-(
  chdir(__DIR__);
  chdir('../');
  //print_r (realpath(__DIR__).' '.getcwd()."\n");
  if (!is_readable('conf.ini') ) trigger_error ('Error loading config file from here '.getcwd()."\n");

  $conf = array();
  $conf = parse_ini_file('conf.ini', true);
  //print_r ($conf);
  if ( !isset( $conf['_database'] ) ) trigger_error ('Error loading config file. No database specified.');
  $dbinfo =& $conf['_database'];
  if ( !is_array( $dbinfo ) || !isset($dbinfo['host']) || !isset( $dbinfo['user'] ) || !isset( $dbinfo['password'] ) || !isset( $dbinfo['name'] ) ) {
    trigger_error ('Error loading config file.  The database information was not entered correctly.');
  }
  $db = mysqli_connect($dbinfo['host'], $dbinfo['user'], $dbinfo['password'], $dbinfo['name'] );
  if ( !$db ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error($db)."\n");

  $debug = 0;  // 1 = ausgabe und keine Mail an User
  $mailto = 0; // 0 = keine Mail an Debugger
  //$mailto = 'schachi@mpi-magdeburg.mpg.de';

  // url
  $url = 'http';
  if ( isset($conf['_own']['ssl'])) {
    if ($conf['_own']['ssl'] == 1) $url = 'https';
  }
  $path = (basename(realpath('./')));
  $host = shell_exec("hostname -f | tr -d '\n'");
  $url  = $url.'://'.$host.'/'.$path;
  if ($debug) print_r($url."\n");


  // loesche alle <table>__history, welche nicht erwuenscht sind
  // Xataface hat nur einen globalen Schalter ON/OFF fuer history, aber wer braucht denn alle histories?
  $sql = "SELECT reiter FROM view_reiter WHERE reiter IN (SELECT CONCAT(lst.reiter, '__history') AS table_his FROM list_reiter AS lst LEFT JOIN view_reiter AS vReit ON lst.reiter = vReit.reiter WHERE lst.history = '0' AND vReit.table_type = 'BASE TABLE' AND lst.reiter NOT LIKE '%__history') AND table_type = 'BASE TABLE';";
  $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    while($row = mysqli_fetch_assoc($result)) {
      $table = $row['reiter'];
      $sql = "DROP TABLE IF EXISTS $table;";
      if ($debug) echo "$sql\n";
      mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    }
  }

  mysqli_close($db);

?>
