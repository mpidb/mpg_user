## Einleitung ##

Diese kleine User-Datenbank dient in erster Linie zum Anlegen von Benutzern, welche in den anderen Datanbanken per View aus Auswahl zur Autorisierung dienen.<br>
Ein Erweiterung ist geplant aber noch nicht angepackt.

## Projekt downloaden ##

**Evtl. vorh. DB und Filesystem vorher sichern**<br>
<br>
Entweder von common/db_export das tar-File herunterladen oder per Befehl
~~~bash
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_user.git
~~~
in das Wurzelverzeichnis des Webserver klonen.

## Installation ##

siehe LIEMICH.txt in Folder install<br>
Dort liegen auch alle noch notwendigen SQL-Skripte fuer die DB-Erstellung.

## Screenshot ##

<a href="install/db_user.png" title="Ueberblick Datenbanken"><img src="install/db_user.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>  
See the GNU General Public License for more details.<br>
See also the file LICENSE.txt here
